/**
 * @license
 * Copyright 2019 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */

import { LitElement, html, css } from 'lit';

/**
 * An example element.
 *
 * @slot - This element has a slot
 * @csspart button - The button
 */
export class MyElement extends LitElement {
    static get styles() {
        return css `
      :host {
        display: flex;
        justify-content: space-between;
        border: solid 1px gray;
        padding: 16px;
        max-width: 1200px;
        height: 700px;
        background-color:rgb(167, 212, 243);
      }
     
    `;
    }

    static get properties() {
        return {
            /**
             * The name to say "Hello" to.
             */
            name: { type: String },

            /**
             * The number of times the button has been clicked.
             */
            count: { type: Number },
        };
    }

    constructor() {
        super();
        this.name = 'World';
        this.count = 0;
    }

    render() {
        return html `
      <slot></slot>
    `;
    }


}

window.customElements.define('my-element', MyElement);