import { css, html, LitElement } from "lit";




export class Impresiones extends LitElement {

    static get properties() {
        return {
            return: { typo: String },
        };
    }

    static get styles() {
        return css `
        input{margin-bottom:1%;
            margin-left:3%;
        }
        hr {
            height: 3px;
            background-color: #1629d8;
            margin-top:1%;
            margin-bottom:1%;
            margin-left:1%;
            float:"left";
            }
            /* #8ed2e4 */
        button{
            width:100%;    
        }
   
        `;
    }

    constructor() {
        super();

    }

    render() {

        return html `

            <h3>Imprimir personas censadas</h3>
      
            <hr />
            <button @click="${()=>this.imprimir("apellido")}">Imprimir por apellido</button>
            <hr />
            <button @click="${()=>this.imprimir("mayores")}">Imprimir indice desocupacion</button>
            <hr />
            <button @click="${()=>this.imprimir("pobre")}">Imprimir indice pobreza</button>
            <hr />
            <button @click="${()=>this.imprimir("sexo")}">Imprimir cantidad por sexo</button>
            <div>${this.return}</div>
        `
    }


    async imprimir(tipo) {

        const respuesta = await fetch(`http://127.0.0.1:8080/Censo2021/exportarArchivo?imprime=${tipo}`, {
                method: "get",
                headers: {
                    "Content-Type": "application/json",
                },
            })
            .then(function(response) {
                return response.json();
            })
            .then(usuario => {
                console.log(usuario)
                this.return = usuario
                return usuario;
            });

        console.log("res: " + respuesta)

    }


}


window.customElements.define('mis-impresiones', Impresiones);