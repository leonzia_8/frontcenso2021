import { css, html, LitElement } from "lit";




export class formularioPersona extends LitElement {

    static get properties() {
        return {
            DNI: { type: Number },
            nombre: { typo: String },
            apellido: { typo: String },
            fechaNacimiento: { typo: String },
            sexo: { typo: String },
            domicilio: { typo: String },
            telefono: { typo: String },
            ocupacion: { typo: String },
            ingresoMensual: { typo: Number },
            return: { typo: String },
        };
    }

    static get styles() {
        return css `
        input{margin-bottom:1%;
            margin-left:3%;
        }
        button{width:100%}
        hr {            
            height: 3px;
            background-color: #1629d8;
            margin-top:1%;
            margin-bottom:1%;
            margin-left:1%;
            float:"left";
            }
        #form{

            background-color:#8ed2e4
        }
        `;
    }

    constructor() {
        super();

    }

    render() {

        return html `

            <div id="form">
            <h3>Formulario Censo 2021</h3>
            <input type="number" @input="${this.handleNombre}"  name="DNI" placeholder="Documento"/>
            <br/>
            <input type="text" @input="${this.handleNombre}"  name="nombre" placeholder="Nombre"/>
            <br/>
            <input type="text" @input="${this.handleNombre}"  name="apellido" placeholder="Apellido"/>
            <br/>
            <input type="text" @input="${this.handleNombre}"  name="fechaNacimiento" placeholder="Fecha de Nacimiento"/>
            <br/>
            <input type="text" @input="${this.handleNombre}"  name="sexo" placeholder="Género"/>
            <br/>
            <input type="text" @input="${this.handleNombre}"  name="domicilio" placeholder="Domicilio"/>
            <br/>
            <input type="text" @input="${this.handleNombre}"  name="telefono" placeholder="Telefono"/>
            <br/>
            <input type="text" @input="${this.handleNombre}"  name="ocupacion" placeholder="Ocupacion"/>
            <br/>
            <input type="number" @input="${this.handleNombre}"  name="ingresoMensual" placeholder="Ingreso Mensual"/>
            <br/>
            <hr />
            <button @click="${this.enviarDatos}">Enviar Datos</button>
            <hr />
            <div>${this.return}</div>
            </div>
        `
    }

    handleNombre(event, tipo = event.target.name) {
        this[tipo] = event.target.value;
    }

    async enviarDatos() {

        var data = {}
        data.DNI = this.DNI;
        data.Nombre = this.nombre;
        data.Apellido = this.apellido;
        data.FechaNacimiento = this.fechaNacimiento;
        data.Sexo = this.sexo;
        data.Direccion = this.domicilio;
        data.Telefono = this.telefono;
        data.Ocupacion = this.ocupacion;
        data.IngresoMensual = this.ingresoMensual;




        const respuesta = await fetch("http://127.0.0.1:8080/Censo2021/PersonaServlet", {
                method: "post",
                body: JSON.stringify(data),
                headers: {
                    "Content-Type": "application/json",
                },
            })
            .then(function(response) {
                return response.json();
            })
            .then(usuario => {
                console.log(usuario)
                this.return = usuario
                window.location.href = window.location.href
                return usuario;
            });

        console.log("res: " + respuesta)

    }


}


window.customElements.define('formulario-persona', formularioPersona);