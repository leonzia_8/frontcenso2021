import { css, html, LitElement } from "lit";


export class TablaPersonas extends LitElement {

    static get properties() {
        return {
            personasCensadas: { typo: Array },
        };
    }

    static get styles() {
        return css `
        input{margin-bottom:1%;
            margin-left:3%;
        }
        #titulo{
            display:flex;
            justify-content:space-around;
            background-color:#8bbcdc;
        }
   
        `;
    }

    constructor() {
        super();

    }



    render() {
            return html `
            <div id="titulo">
            <h3>Personas censadas</h3>
            </div>
            <div>
               <input type="radio" name="vistas" value="pobre" @click=${()=>this.imprimir("pobre")}/>Imprimir pobreza
               <input type="radio" name="vistas" value="mayores" @click=${()=>this.imprimir("mayores")}/>Imprimir desocupados
               <input type="radio" name="vistas" value ="apellido" @click=${()=>this.imprimir("apellido")}/>Imprimir por apellido
               <input type="radio" name="vistas" value="sexo" @click=${()=>this.imprimir("sexo")}/>Imprimir por sexo
            </div> 
            
            <hr />
            <p>Para borrar seleccione el id de la tabla.</p>
            <div>
            <table BORDER class="default">
            <tr>
                <th>ID</th>
                <th>Documeto</th>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Nacimiento</th>
                <th>Sexo</th>
                <th>Direccion</th>
                <th>Telefono</th>
                <th>Ocupacion</th>
                <th>Ingresos</th>

            </tr>

            ${this.personasCensadas ? this.personasCensadas.map(x=>{
                return html `
                    <tr>
                        <th @click=${this.getId}>${x.id}</th>
                        <td>${x.dni}</td>
                        <td>${x.nombre}</td>
                        <td>${x.apellido}</td>
                        <td>${x.fechaNacimiento}</td>
                        <td>${x.sexo}</td>
                        <td>${x.direccion}</td>
                        <td>${x.telefono}</td>
                        <td>${x.ocupacion}</td>
                        <td>${x.ingresoMensual}</td>
                    </tr>
                `
            }) : null}
            </table>
            </div>
        `
    }

    connectedCallback() {
        super.connectedCallback();
        this.imprimir()
    }

    async imprimir(tipo) {


        const respuesta = await fetch(`http://127.0.0.1:8080/Censo2021/PersonaServlet?imprime=${tipo ? tipo : "default"}`, {
                method: "get",
                headers: {
                    "Content-Type": "application/json",
                },
            })
            .then(function(response) {
                return response.json();
            })
            .then(data => {
                this.personasCensadas = data  
                console.log(data)

                return data;
            });
 

    }


    async getId(event){
       var borra =  confirm("desea eliminar el id " + event.target.textContent, + "?")

       if(borra){
        const respuesta = await fetch(`http://127.0.0.1:8080/Censo2021/PersonaServlet?id=${event.target.textContent}`, {
            method: "delete",
            headers: {
                "Content-Type": "application/json",
            },
        })
        .then(function(response) {
            return response.json();
        })
        .then(data => {
            this.return = data
            alert("El Id seleccionado ha sido borrado.")
            window.location.href = window.location.href
            return data;
        });


       } 

    }

}


window.customElements.define('tabla-personas', TablaPersonas);